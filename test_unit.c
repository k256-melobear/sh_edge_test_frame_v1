#include "test.h"
#include "sh_alm_test15_1.h"
#include "edge_alarm.h"
#include "buffer.h"
/*测试报警*/
int32_t ai_cache[EDGE_CACHE_LEN];

char output_param_pack[DEVICE_DATA_LEN];
//void test_alarm(){
//    DeviceParameter device_parameter={
//            .device_id =123,
//            .point_num =1,
//            .fe_time={20,21,11,11,11,11},
//    };
//    PointParameter point_parameter[1]={0};
//    point_parameter[0].point_id =1;
//    point_parameter[0].type_point =0,
//    point_parameter[0].channel_num=2,
//    point_parameter[0].fe_value.ha_rms.fe_value =10;
//    char output_param_pack[DEVICE_DATA_LEN];
//    edge_ai(&device_parameter, point_parameter, output_param_pack, buffer, ai_cache);
//}

void test_running() {
    /*准备测试数据*/

	UnitBuffer alm_buffer;
	uint8_t a = (int)buffer % 4;
	if (a == 0){
		alm_buffer.buff_float = (float *)(buffer+4);
	}else{
		memcpy(&buffer[a],&buffer[4],EDGE_BUFF_LEN - 4 + a);
		alm_buffer.buff_float = (float *)(buffer + a);
	}
//		  alm_buffer->buff_float = (float *)buffer;
	PointParameter pointmodel[POINT_NUMB] = { 0 };
	DeviceParameter devicemodel = { 0 };
    devicemodel.station_param_body = (StationParamBody *)devicemodel.input_param_pack;
    devicemodel.station_param_body->cyz_interval = 5;
	devicemodel.station_param_body->code = 548;
	devicemodel.point_num = POINT_NUMB;
	devicemodel.device_id = 344;

	for (int it = 0; it < INDEX_NUMB; it++) {
		for (int i = 0; i < 6; i++){
		devicemodel.fe_time[i] = points_t[it * 6 + i];
		}
		devicemodel.out_alarm_level = 0;
        devicemodel.alarm_level_edge = 0;
		devicemodel.out_running_state = sb_stopbin[it];
		for (int ipt = 0; ipt < devicemodel.point_num; ipt++) {
		    pointmodel[ipt].point_id = ipt + 1;
		    pointmodel[ipt].fe_type = 1;
			pointmodel[ipt].channel_num = ipt;
			pointmodel[ipt].type_point = type_sensor[ipt];
            pointmodel[ipt].station_point_param = (StationPointParam *)(devicemodel.input_param_pack + sizeof(StationParamBody));
			//pointmodel[ipt].alm_level = 0;
			pointmodel[ipt].station_point_param->cdloc = cdlocs[ipt];
			//pointmodel[ipt].stop_state = 1;
			pointmodel[ipt].fe_value.vel_rms.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + VelRms];
			pointmodel[ipt].fe_value.la_rms.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + LaRms];
			pointmodel[ipt].fe_value.ha_rms.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + HaRms];
			pointmodel[ipt].fe_value.ha_imp.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Ha_imp];
			pointmodel[ipt].fe_value.la_imp.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + La_imp];
			pointmodel[ipt].fe_value.vel_homo_num.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Vel_homo_num];
			pointmodel[ipt].fe_value.vel_homo_eng.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Vel_homo_eng];
			pointmodel[ipt].fe_value.temperature.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Temperature];
			pointmodel[ipt].fe_value.ha_env_eng.fe_value = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Ha_bearing_eng];
			pointmodel[ipt].fe_value.vel_1x_eng = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Vel_1x];
			pointmodel[ipt].fe_value.vel_2x_eng = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Vel_2x];
            pointmodel[ipt].fe_value.ha_lubricate = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Ha_lubricate];
            pointmodel[ipt].fe_value.ha_bcj_score = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Ha_bcj_score];
            pointmodel[ipt].fe_value.ha_narrow_eng = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Ha_narrow_eng];
            pointmodel[ipt].fe_value.la_narrow_eng = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + La_narrow_eng];
            pointmodel[ipt].fe_value.vel_blade_eng = points_fe[it* POINT_NUMB * EDGE_INDEX_NUMB + ipt * EDGE_INDEX_NUMB + Vel_blade_eng];

			pointmodel[ipt].fe_value.vel_rms.out_alarm_level = 0;
			pointmodel[ipt].fe_value.la_rms.out_alarm_level = 0;
			pointmodel[ipt].fe_value.ha_rms.out_alarm_level = 0;
			pointmodel[ipt].fe_value.ha_imp.out_alarm_level = 0;
			pointmodel[ipt].fe_value.la_imp.out_alarm_level = 0;
			pointmodel[ipt].fe_value.vel_homo_num.out_alarm_level = 0;
			pointmodel[ipt].fe_value.vel_homo_eng.out_alarm_level = 0;
			pointmodel[ipt].fe_value.temperature.out_alarm_level = 0;
			pointmodel[ipt].fe_value.ha_env_eng.out_alarm_level = 0;
		}

        edge_alm(&devicemodel, pointmodel, &alm_buffer, ai_cache, 1);
        if (devicemodel.out_alarm_level > 0 || devicemodel.alarm_level_edge > 0) {
            debug_printf("/********* %d **********:/\r\n", it + 1);
            debug_printf(" %d     %d \r\n", devicemodel.out_alarm_level, devicemodel.alarm_level_edge);
        }
			}
	printf("#ifndef __BUFFER_H__\r\n");
	printf("#define __BUFFER_H__\r\n");
	printf("#include \"edge_ai.h\"\r\n");
    printf("char buffer[EDGE_BUFF_LEN] = { ");
    for (int i = 0; i < EDGE_BUFF_LEN - 1; ++i) {
      printf("%d,",buffer[i]);
  }
    printf("0 };\r\n");
    printf("#endif\r\n");
//				    DeviceParameter device_parameter;
//    StationParamBody station_p_body;
//    device_parameter.station_param_body = (StationParamBody *)&station_p_body;
//    device_parameter.station_param_body->register_fun.running_state=2;    //启用停机不采
//    device_parameter.station_param_body->cyz_interval = 9;
//    device_parameter.point_num = 3;
//		device_parameter.device_id=	196;
//		StationParamHead *station_para_head = (StationParamHead *)device_parameter.input_param_pack;
//		StationParamBody *station_para_body = (StationParamBody *)(device_parameter.input_param_pack + sizeof(StationParamHead));
//		station_para_head->version = 1;
//		station_para_head->device_param_len = DEVICE_PARAM_LEN;
//		station_para_body->code = 2;
//		station_para_body->cyz_interval = 9;
//		station_para_body->device_id = 196;
//		station_para_body->point_num = 3;
//		station_para_body->register_fun.running_state = 2;
//		StationPointParam *station_point_param = (StationPointParam *)(device_parameter.input_param_pack + sizeof(StationParamHead) + sizeof(StationParamBody));
//		for (int i_point = 0; i_point < device_parameter.point_num; i_point++) {
//				station_point_param->cdloc = i_point+1;
//				station_point_param->channel_num = 180;
//				station_point_param->index = i_point+1;
//				station_point_param->point_id = i_point + 130;
//				station_point_param->stop_thd = 1888.10002;
//				station_point_param->be_justified = 0;
//				station_point_param = (StationPointParam *)((char *)station_point_param + sizeof(StationPointParam));
//		}
//		station_para_head->check = send_check_sum((char *)station_para_body, DEVICE_PARAM_LEN - sizeof(StationParamHead));
//    StationPointParam station_p_para[3];
//    PointParameter point_parameter[3];

//    for (int i_point = 0; i_point < device_parameter.point_num; i_point++) {
//        point_parameter[i_point].station_point_param = (StationPointParam *)&station_p_para[i_point];
//        point_parameter[i_point].type_point = 0;
//        point_parameter[i_point].point_id = i_point + 130;
//        point_parameter[i_point].station_point_param->cdloc = 1;
//        point_parameter[i_point].station_point_param->stop_thd = 3;
//        point_parameter[i_point].fe_value.ha_err_sig = 0;
//        point_parameter[i_point].station_point_param->index=2;
//        point_parameter[i_point].station_point_param->be_justified = 0;
//        point_parameter[i_point].fe_value.ha_rms.fe_value = 8;
//    }
//		point_parameter[0].station_point_param->stop_thd = 1888.1002;
//    memset(ai_cache,0,sizeof(int) * EDGE_CACHE_LEN);
//    memset(buffer, 0, sizeof(char) * EDGE_BUFF_LEN);
//    char output_param_pack[DEVICE_DATA_LEN];
//    short a[301]={0, 5898.06, 5898.06, 5898.06, 5898.06, 5898, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34};
//    memcpy(buffer, a, sizeof(a));
//    edge_ai(&device_parameter, point_parameter, output_param_pack, buffer, ai_cache);
//		printf("1");
}
