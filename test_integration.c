//#include "test.h"
#include "device_param_pack.h"
#include "test_1_10.h"
//#include "alr_test1.h"
#include "edge_ai.h"
#include "get_param_head.h"
#include "test.h"
#include "RozhSystem.h"
#include "public.h"
#include "RTC_App.h"

#define CHANGE_DEVICE_TYPE
//#define CHANGE_POINT_NUM
/*发送方和校验*/
//#define TEST_ERRPOINT
static uint8_t send_check_sum(char *buf, int32_t len)
{
    int32_t i;
    uint8_t ret= 0;
    for(i=0; i<len; i++)
    {
        ret += *(buf++);
    }
    ret = ~ret;
    return ret;
}
/*测试报警*/

//void test_alarm(){
//    DeviceParameter device_parameter={
//            .device_id =123,
//            .point_num =1,
//            .fe_time={20,21,11,11,11,11},
//    };
//    PointParameter point_parameter[1]={0};
//    point_parameter[0].point_id =1;
//    point_parameter[0].type_point =0,
//    point_parameter[0].channel_num=2,
//    point_parameter[0].fe_value.ha_rms.fe_value =10;
//    char output_param_pack[DEVICE_DATA_LEN];

//    edge_ai(&device_parameter, point_parameter, output_param_pack, buffer, ai_cache);
//}
//#define MINING_TIME
int32_t ai_cache[EDGE_CACHE_LEN] = {0};
char buffer[EDGE_BUFF_LEN] = {0};
char buffer2[EDGE_BUFF_LEN] = {0};
DeviceParameter device_parameter;
PointParameter point_parameter[27];
void inte_test() {
    /*准备测试数据*/
	uint32_t buffer_len;
	for(int round_i = 0; round_i < history_buf_len;round_i++){
		int point_num = point_number;
		#ifdef CHANGE_POINT_NUM
		if(round_i == 3)
		{
			point_num = 2;
		}
		#endif
//    DeviceParameter device_parameter;
		printf("----------test round is %d----------\n",round_i+1);
    device_parameter.point_num = point_num;
		device_parameter.device_id=	196;
//		for(int time_index = 0;time_index < 6;time_index ++ ){
//			device_parameter.fe_time[time_index] = points_t[round_i][time_index];
//		}
		StationParamHead *station_para_head = (StationParamHead *)device_parameter.input_param_pack;
		StationParamBody *station_para_body = (StationParamBody *)(device_parameter.input_param_pack + sizeof(StationParamHead));
		station_para_head->version = 1;
		station_para_head->device_param_len = DEVICE_PARAM_LEN;
		station_para_body->code = 548;
		#ifdef CHANGE_DEVICE_TYPE
		if(round_i > 10)
			station_para_body->code = 2;
		#endif
		station_para_body->cyz_interval = 5;
		station_para_body->device_id = 196;
		station_para_body->point_num = point_num;
		station_para_body->register_fun.running_state = 1;
		StationPointParam *station_point_param = (StationPointParam *)(device_parameter.input_param_pack + sizeof(StationParamHead) + sizeof(StationParamBody));
	#ifdef MINING_TIME
		char order_mining_begin[6] = {21,8,20,0,0,0};
		char order_mining_end[6] = {04,03,0,0,59,59};
		uint8_t fe_time[6]= {21,8,20,0,round_i*15 + 1,0};
		memcpy(station_para_body->order_mining_begin,order_mining_begin,sizeof(order_mining_begin));
		memcpy(station_para_body->order_mining_end,order_mining_end,sizeof(order_mining_end));
		memcpy(device_parameter.fe_time,fe_time,sizeof(fe_time));
	#endif
		for (int i_point = 0; i_point < point_num; i_point++) {
				station_point_param->cdloc = cdloc[i_point];
				station_point_param->channel_num = i_point+1;
				station_point_param->index = pc_trust[i_point];
				station_point_param->point_id = i_point+1;
				station_point_param->stop_thd = pc_thd[i_point];
				station_point_param->stop_thd = 1888.1;
				station_point_param->be_justified = is_manual[i_point];

		#ifdef TEST_MULTIPOINT
				if(i_point == 1 || i_point == 2)
			{
				station_point_param->cdloc = cdloc[i_point];
				station_point_param->channel_num = i_point+1;
				station_point_param->index = pc_trust[i_point];
				station_point_param->point_id = i_point+1;
				station_point_param->stop_thd = pc_thd_pump[i_point];
				station_point_param->be_justified = is_manual[i_point];
			}
		#endif
		#ifdef TEST_CHANGEPOINT
			if(round_i == history_buf_len - 5)
				if(i_point != 0)
				{
					station_point_param->cdloc = cdloc[i_point];
					station_point_param->channel_num = i_point+10;
					station_point_param->index = pc_trust[i_point];
					station_point_param->point_id = i_point+10;
					station_point_param->stop_thd = pc_thd[i_point];
					station_point_param->be_justified = is_manual[i_point];
				}
		#endif	
				station_point_param = (StationPointParam *)((char *)station_point_param + sizeof(StationPointParam));
		}
		station_para_head->check = send_check_sum((char *)station_para_body, DEVICE_PARAM_LEN - sizeof(StationParamHead));

   

    for (int i_point = 0; i_point < 27; i_point++) {

        point_parameter[i_point].type_point = 0;
        point_parameter[i_point].point_id = i_point+1;
        point_parameter[i_point].fe_value.ha_err_sig = 0;
				point_parameter[i_point].channel_num = i_point+1;
			
				point_parameter[i_point].fe_type = 1;
        point_parameter[i_point].fe_value.ha_rms.fe_value = history_buf[round_i];   //history_buf[round_i]
//				if(round_i%2 ==0)
//				{
//					point_parameter[i_point].fe_type = 1; 
//				}
				if(round_i%1000 ==0)
				{
					SystemFeedDog();  
				}
				#ifdef TEST_MULTIPOINT
					if(i_point == 0)
					{
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].point_id = i_point+1;
						point_parameter[i_point].fe_value.ha_err_sig = 0;
						point_parameter[i_point].channel_num = i_point+1;
						point_parameter[i_point].fe_type = 1;

						point_parameter[i_point].fe_value.ha_rms.fe_value = history_buf[round_i];
					}
					if(i_point == 1)
					{
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].point_id = i_point+1;
						point_parameter[i_point].fe_value.ha_err_sig = 0;
						point_parameter[i_point].channel_num = i_point+1;
						point_parameter[i_point].fe_type = 1;

						point_parameter[i_point].fe_value.ha_rms.fe_value = pump_history_buf[round_i];
					}
					if(i_point == 2)
					{
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].point_id = i_point+1;
						point_parameter[i_point].fe_value.ha_err_sig = 0;
						point_parameter[i_point].channel_num = i_point+1;
						point_parameter[i_point].fe_type = 1;

						point_parameter[i_point].fe_value.ha_rms.fe_value = pump_history_buf[round_i];
					}
				#endif
				#ifdef TEST_ERRPOINT
					if(i_point == 1)
					{
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].point_id = i_point+1;
						point_parameter[i_point].fe_value.ha_err_sig = 0;
						point_parameter[i_point].channel_num = i_point+1;
						point_parameter[i_point].fe_type = 1;
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].fe_value.ha_rms.fe_value = history_buf[round_i];
					}
					if(i_point == 2)
					{
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].point_id = i_point+1;
						point_parameter[i_point].fe_value.ha_err_sig = 1;
						point_parameter[i_point].channel_num = i_point+1;
						point_parameter[i_point].fe_type = 1;
						point_parameter[i_point].type_point = 0;
						point_parameter[i_point].fe_value.ha_rms.fe_value = pump_history_buf[round_i];
					}
				#endif
				#ifdef TEST_CHANGEPOINT
					if(round_i == history_buf_len - 5)
						if(i_point != 0)
						{
							point_parameter[i_point].type_point = 0;
							point_parameter[i_point].point_id = i_point+10;
							point_parameter[i_point].fe_value.ha_err_sig = 0;
							point_parameter[i_point].channel_num = i_point+10;
							point_parameter[i_point].fe_type = 1;
							point_parameter[i_point].type_point = 0;
							point_parameter[i_point].fe_value.ha_rms.fe_value = history_buf[round_i];
						}
				#endif	
    }

//    char output_param_pack[DEVICE_DATA_LEN];
//    short a[301]={0, 5898.06, 5898.06, 5898.06, 5898.06, 5898, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 5898.06, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34, 655.34};
//    memcpy(buffer, a, sizeof(a));
		if(round_i == 3000){
			printf("stop\n");
		}
		if(round_i == 392){
			printf("stop\n");
		}
		if(round_i == 120){
			printf("stop\n");
		}
		int c_status = 0;
		memcpy(buffer, buffer2, sizeof(buffer));
    c_status = edge_ai(&device_parameter, point_parameter, buffer, ai_cache, &buffer_len);
		if (device_parameter.buff_save) {
			memcpy(buffer2, buffer, sizeof(buffer));
		}
//		if(round_i == 1500){
//			SystemFeedDog();
//			printf("stop\n");
//		}
//		if(round_i == 130){
//			printf("stop\n");
//		}
//		if(round_i == 1000){
//			printf("stop\n");
//		}
//		if(round_i == 2000){
//			printf("stop\n");
//		}
			uint8_t status;
			uint8_t collection;
			status = device_parameter.out_running_state;
			collection = device_parameter.out_collection_signal;
			int f_ws = device_parameter.out_working_mining;
			int device_status = device_parameter.out_device_mining;
			printf("%d\n",c_status);
		if(0 == status)
			{
				printf("running\n");
			}
			if(1 == status)
			{
				printf("stop\n");
			} 
			if(0 == collection)
			{
				printf("mining\n");
			}
			if(1 == collection)
			{
				printf("low power collection\n");
			} 
			switch(f_ws){
				case 0:
					printf("normally collect\n");
					break;
				case 1:
					printf("frequently change status\n");
					break;
				case 2:
					printf("mining protection and upload\n");
					break;
				case 3:
					printf("start and addionalcollection\n");
					break;
				case 4:
					printf("keep mining\n");
					break;
			}		
			switch(device_status){
				case 0:
					printf("normally collect\n");
					break;
				case 1:
					printf("alarm protection\n");
					break;
				case 2:
					printf("alarm protection and upload\n");
					break;
				case 3:
					printf("alarm protection and additional collection\n");
					break;

			}	
		
	}
//			printf("1");
}
//
//
//
//
