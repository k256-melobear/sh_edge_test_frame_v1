
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include "bsp.h"
#include "efm_it.h"
#include "sampledata.h"
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include "sh_edge.h"
#include "wave_16K.h"   //la_8   ha_4
//#include "la_testw1.h"   //la_8   ha_4
#include "param_set_multip.h"
//#include "alr_test1.h"
#define sample_num 52
#define fe_type 1// 指标计算类型0:常规 ；1:超级指标；2:测量定义指标
//#define TEST_ALARM
//测试函数
int Bufshort[12832] = {0};
GlobalHaParameter IniGlobalHaLoss;
GlobalLaParameter IniGlobalLaLoss;
AlarmBuf alarmbuf[EDGE_INDEX_NUMB] ={0}; 
EdgeFeature edge_features = {0};
//short frange[] = { 10,100,100,1000 };
void Calc(void)//边缘计算
{

// AlarmBuf alarmbuf[EDGE_INDEX_NUMB] ={0}; 
  for(int round_i = 0; round_i < sample_num; round_i ++){
    if(round_i == 30)
      printf("stop\n");
    ErrorBuf errorbuf = {0};

    int typefe = fe_type;
		
    ConfigAlg config_alg = {
      .feature_len = 30,
      .alarm_type = 1    //1：变点检测   2:固定门限报警
    };
    for(int i = 0;i<30;i++){
      config_alg.feature_list[i] = i;
    }
    int i = 0;
    int halen;
    int lalen;
    int vlen;
//    if (typefe ==2)
//    {
        halen = 16384;  //16384
//        lalen = 8192;  //16384
//        vlen = 4096;    //8192
//    }
//      if (typefe ==1)
//    {
//        halen = 16384;
        lalen = 4096;   //4096
        vlen = lalen;    //4096
//    }
//      if (typefe ==0)
//    {
//        halen = 1024;
//        lalen = 512;
//        vlen = 512;
//    }
    
//    if (fs <= 5120 & typefe <= 1) {
//      i = 12;
//  //    datacoe=datacoe4096;
//      fs=2560;
//    }
//    if (fs >10000 & typefe==0) {
//      i=9;  
//    }
    init_param(round_i);
    
    IniGlobalHaLoss.point_code = 1;
    IniGlobalHaLoss.fs =fs;//采样频率
    IniGlobalHaLoss.fm = fs/2.56;
    IniGlobalHaLoss.len_ha = halen;
    IniGlobalHaLoss.t_sin = t_sin;//
    IniGlobalHaLoss.bias_voltage_error = 0;//偏置电压异常
    IniGlobalHaLoss.t_second = 1000+60*round_i;
    IniGlobalHaLoss.intensive_flag = 1;//加密采集的波形标记
    IniGlobalHaLoss.config_alg = config_alg;
    IniGlobalHaLoss.data_coe = datacoe;
    IniGlobalHaLoss.type_fe = typefe;
    IniGlobalHaLoss.param_pack_valid = 1;
//    if(round_i == 2)
//    {
//        IniGlobalHaLoss.data_coe = datacoe * 30;
//    }    
#ifdef HA_abnormal_test
    IniGlobalHaLoss.data_coe = datacoe * alarm_coe_ha[round_i];
#endif
//    memcpy(IniGlobalHaLoss.param_pack, cache_p1 ,PARAM_PACK_SIZE*2);
    
    //float datacoe;
    IniGlobalLaLoss.point_code = 1;
    IniGlobalLaLoss.fs = fs;//采样频率
    IniGlobalLaLoss.len_d_la = lalen;
    IniGlobalLaLoss.len_d_v = vlen;
    IniGlobalLaLoss.fm =fs/2.56 ;    //fs/2.56
    IniGlobalLaLoss.t_sin = t_sin;
    IniGlobalLaLoss.type_fe = typefe;
    IniGlobalLaLoss.bias_voltage_error = 0;//偏置电压异常
    IniGlobalLaLoss.type_sensor = 0;
    IniGlobalLaLoss.t_second = 1000+60*round_i;
    IniGlobalLaLoss.intensive_flag = 1;//加密采集的波形标记
    IniGlobalLaLoss.config_alg = config_alg;
    IniGlobalLaLoss.data_coe = datacoe;
    
#ifdef LA_abnormal_test
    IniGlobalLaLoss.data_coe = datacoe * alarm_coe[round_i];
#endif
    
    IniGlobalLaLoss.param_pack_valid = 1; 
//    uint8_t p[] = {0x0,0x10,0x0,0x0,0x0,0x10,0x0,0x0,0x0,0x0,0x20,0x45,0x0,0x0,0x7a,0x44,0x58,0x28,0xab,0x3a,0x0,0x0,0x0,0x0,0xa8,0x2f,0x1,0x10,0x2,0x1,0x98,0x23,0x24,0x2,0x0,0x0,0x66,0x9,0x60,0x9,0x1,0x2,0x5,0xa,0x3,0x1,0x1,0x0,0x0,0x0,0x3,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x41,0x3,0x1,0x2,0x8,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x59,0x6,0x2,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0x0,0xf3,0x3,0x0,0x0,0x2,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x0,0x12,0x0,0x0,0x1,0x0,0x0,0x0,0x1,0x0,0x0,0x0,0x30,0xe7,0x4f,0x0,0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xb,0xc,0xd,0xe,0xf,0x10,0x11,0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1,0x1,0x0,0x0,0x40,0x0,0x1,0x1,0x0};
//    memcpy(&IniGlobalLaLoss, p,sizeof(p));
    memcpy(IniGlobalLaLoss.param_pack, cache_p1 ,PARAM_PACK_SIZE*2);
    float temperature =  20;
    
//    if(round_i == 2)
//    {
//        IniGlobalLaLoss.data_coe = datacoe * 0.09;
//    }   
    if(round_i == 9) 
      printf("stop\n");
    printf("round %d result:\n",round_i + 1);
    g_delayTime = 0;
    sh_edge_la(waveData, Bufshort, &IniGlobalLaLoss, &edge_features, &errorbuf, alarmbuf, temperature,0);
    printf("delay time is %d\n",g_delayTime);
//    float vel_err = fabs(fe[i]-edge_features.vel_rms.fe_value)/fe[i];
//    float la_err = fabs(fe[i+1]-edge_features.la_rms.fe_value)/fe[i+1];
//    float lbandulow_err = fabs(fe[i+2]-edge_features.la_band_ultralow)/fe[i+2];
//    float lbandlow_err = fabs(fe[i+3]-edge_features.la_band_low)/fe[i+3];
//    float vel_homo_num_err = fabs(fe[i+4]-edge_features.vel_homo_num.fe_value)/fe[i+4];
//    float vel_homo_eng_err = fabs(fe[i+5]-edge_features.vel_homo_eng.fe_value)/fe[i+5];
//    float la_imp_err = fabs(fe[i+6]-edge_features.la_imp.fe_value)/fe[i+6];
//    float vel_1x_eng_err = fabs(fe[i+7]-edge_features.vel_1x_eng)/fe[i+7];
//    float vel_1x_eng_ratio_err = fabs(fe[i+8]-edge_features.vel_1x_eng_ratio)/fe[i+8];
//    float vel_2x_eng_err =  fabs(fe[i+9]-edge_features.vel_2x_eng)/fe[i+9];
//    float vel_2x_eng_ratio_err =  fabs(fe[i+10]-edge_features.vel_2x_eng_ratio)/fe[i+10];
//    float vel_rpm_hz_err = fabs(fe[i+11]-edge_features.vel_rpm_hz)/fe[i+11];
//    if(typefe == 2 && fs == 5120)
//    {
//      float la_rms_overall_err = fabs(fe[i+1]-edge_features.la_rms_overall)/fe[i+1];
//      printf("%s  %s\n","低加测量定义理论值","测试值");
//      printf("%s%f  %f  %f\n","la_err:",fe[i+1],edge_features.la_rms_overall,la_rms_overall_err);
//    }
//    else{
//      printf("%s  %s\n","低加指标理论值","测试值");
//      printf("%s%f  %f  %f\n","vel_err:",fe[i],edge_features.vel_rms.fe_value,vel_err);
//      printf("%s%f  %f  %f\n","la_err:",fe[i+1],edge_features.la_rms.fe_value,la_err);
//      printf("%s%f  %f  %f\n","lbandulow_err:",fe[i+2],edge_features.la_band_ultralow, lbandulow_err);
//      printf("%s%f  %f  %f\n","lbandlow_err:",fe[i+3],edge_features.la_band_low, lbandlow_err);
//      printf("%s%f  %f  %f\n","vel_homo_num_err:",fe[i+4],edge_features.vel_homo_num.fe_value, vel_homo_num_err);
//      printf("%s%f  %f  %f\n","vel_homo_eng_err:",fe[i+5],edge_features.vel_homo_eng.fe_value, vel_homo_eng_err);
//      printf("%s%f  %f  %f\n","la_imp_err:",fe[i+6],edge_features.la_imp.fe_value, la_imp_err);
//      printf("%s%f  %f  %f\n","vel_1x_eng_err:",fe[i+7],edge_features.vel_1x_eng, vel_1x_eng_err);
//      printf("%s%f  %f  %f\n","vel_1x_eng_ratio_err:",fe[i+8],edge_features.vel_1x_eng_ratio, vel_1x_eng_ratio_err);
//      printf("%s%f  %f  %f\n","vel_2x_eng_err:",fe[i+6],edge_features.vel_2x_eng, vel_2x_eng_err);
//      printf("%s%f  %f  %f\n","vel_2x_eng_ratio_err:",fe[i+7],edge_features.vel_2x_eng_ratio, vel_2x_eng_ratio_err);
//      printf("%s%f  %f  %f\n","vel_rpm_hz_err:",fe[i+8],edge_features.vel_rpm_hz, vel_rpm_hz_err);
//    }
//    g_delayTime = 0;
//    sh_edge_ha(waveData, Bufshort, &IniGlobalHaLoss, &edge_features, &errorbuf, alarmbuf, temperature,0);   
//    printf("delay time is %d\n",g_delayTime);
//    float ha_rms_err = fabs(fe[i]-edge_features.ha_rms.fe_value)/fe[i];
//    float ha_band_low_err = fabs(fe[i+1]-edge_features.ha_band_low)/fe[i+1];
//    float ha_band_median_err = fabs(fe[i+2]-edge_features.ha_band_median)/fe[i+2];
//    float ha_band_high_err = fabs(fe[i+3]-edge_features.ha_band_high)/fe[i+3];
//    float ha_band_ultrahigh_err = fabs(fe[i+4]-edge_features.ha_band_ultrahigh)/fe[i+4];
//    float ha_imp_err = fabs(fe[i+5]-edge_features.ha_imp.fe_value)/fe[i+5];
//    float ha_env_eng_err = fabs(fe[i+6]-edge_features.ha_env_eng)/fe[i+6];
//    float ha_lubrication_err = fabs(lubrication -edge_features.ha_lubrication)/lubrication ;
//    float ha_kurt_err = fabs(fe[i+8]-edge_features.ha_kurt)/fe[i+8];
////    int l_error_err = abs(h_error - edge_features.ha_err_sig);
////    printf("%s  %s\n","高加指标理论值","测试值");
//    printf("%s%f  %f  %f\n","ha_rms:",fe[i],edge_features.ha_rms.fe_value,ha_rms_err);
//    printf("%s%f  %f  %f\n","ha_band_low:",fe[i+1],edge_features.ha_band_low,ha_band_low_err);
//    printf("%s%f  %f  %f\n","ha_band_median:",fe[i+2],edge_features.ha_band_median, ha_band_median_err);
//    printf("%s%f  %f  %f\n","ha_band_high:",fe[i+3],edge_features.ha_band_high, ha_band_high_err);
//    printf("%s%f  %f  %f\n","ha_band_ultrahigh:",fe[i+4],edge_features.ha_band_ultrahigh, ha_band_ultrahigh_err);
//    printf("%s%f  %f  %f\n","ha_imp:",fe[i+5],edge_features.ha_imp.fe_value, ha_imp_err);
////    printf("%s%f  %f  %f\n","ha_env_eng:",fe[i+6],edge_features.ha_env_eng, ha_env_eng_err);
//    printf("%s%f  %f  %f\n","ha_lubrication:",lubrication,edge_features.ha_lubrication, ha_lubrication_err);
//    printf("%s%f  %f  %f\n","ha_kurt:",fe[i+8],edge_features.ha_kurt, ha_kurt_err);
 }
    return;
  
}
  

