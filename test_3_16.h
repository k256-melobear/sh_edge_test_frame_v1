 //停机加采：采样值第一个点低于2，第二个点高于2，第三四个点低于2
 //加采标记：只有第二个点是1
 #define point_number 3
 #define history_buf_len 4
 float history_buf[history_buf_len] ={1,5,1,1};
 int is_manual[point_number] = {1,1,1};//是否人为修改
 float pc_thd[point_number] = {2,2,2};//上位机门限
 int pc_trust[point_number] = {1,1,1};//上位机置信度，1可信，0不可信
 int cdloc[point_number] = {1,1,1};//电机为1，机泵为4