//
// Created by Administrator on 2021/5/18.
//

#ifndef _PARAM_PACK_STATION_H_
#define _PARAM_PACK_STATION_H_
#include <stdint.h>
#include "get_param_head.h"
#define DEVICE_PARAM_VERSION  1 //本头文件参数版本号
#pragma pack(1)
typedef struct{
    uint8_t running_state;   //启用停机不采
}RegisterFun;
typedef struct StationPointParam{
    uint32_t point_id;	   //测点id
    float stop_thd;
    float index;
    int8_t channel_num;    //测点通道号
    int8_t cdloc;
    int8_t be_justified;
    int8_t res[1];          //参数对齐
}StationPointParam;
typedef struct StationParamBody{
    uint32_t device_id;			//设备id
    uint32_t code;	            //设备类型
    uint8_t point_num;			//测点数量
    uint8_t run_tag;            //设备重复启停标签
    uint8_t cyz_interval;       //采样值采集间隔
    RegisterFun register_fun;   //是否启用某些函数
    char overhaul_time[6];      //检修时间
    char order_mining_begin[6]; //指令触发加采起始时间 TODO
    char order_mining_end[6];	//指令触发加采结束时间 TODO
    int8_t res[2];              //结构体对齐
}StationParamBody;
#pragma pack()
#endif //_PARAM_PACK_STATION_H_
