 //输出 停机
 #define point_number 3
 #define history_buf_len 1 //一个测点误信号，一个测点没有值，一个测点正常 误信号测点高于2，正常测点低于2
 float history_buf[history_buf_len]  ={1};//正常测点
 float no_history_buf[history_buf_len]={0};//没有值
 float pump_history_buf[history_buf_len]={5};//误信号测点
 int is_manual[point_number] = {1,1,1};//是否人为修改  是
 float pc_thd[point_number] = {2,2,2};//上位机门限
 int pc_trust[point_number] = {1,1,1};//上位机置信度，1可信，0不可信
 int cdloc[point_number] = {1,1,1};//电机为1，机泵为4  